<?php

namespace Drupal\premium_node\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'premium_node_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'premium_node.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('premium_node.settings');

    $form['messages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Restriction Messages'),
      '#description' => $this->t('You may customize the messages displayed to unprivileged users trying to view full premium contents.'),
    ];
    $form['messages']['default_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default message'),
      '#description' => $this->t('This message will apply to all content types with blank messages below.'),
      '#default_value' => $config->get('default_message'),
      '#required' => TRUE,
    ];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $content_type) {
      $form['messages']['message_' . $content_type->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Message for %type content type', ['%type' => $content_type->label()]),
        '#default_value' => $config->get('messages.' . $content_type->id()),

      ];
    }

    $options = [];
    $exclude_arr = $this->entityDisplayRepository->getViewModes('node');
    unset($exclude_arr["rss"], $exclude_arr["search_index"], $exclude_arr["search_result"]);

    foreach ($exclude_arr as $id => $view_mode) {

      $options[$id] = $view_mode['label'];

    }

    $form['teaser_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Teaser display mode'),
      '#description' => $this->t('Teaser display view mode to render for content restriction contents.'),
      '#default_value' => $config->get('teaser_view_mode'),
      '#options' => $options,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('premium_node.settings')
      ->set('default_message', $values['default_message'])
      ->set('teaser_view_mode', $values['teaser_view_mode'])
      ->save();
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $content_type) {
      $this->config('premium_node.settings')
        ->set('messages.' . $content_type->id(), $values['message_' . $content_type->id()])
        ->save();
    }
    parent::submitForm($form, $form_state);
  }

}
