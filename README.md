# Premium Node

This module is used to show premium node specific node.
This module will be used for every node restrict summary purposes only.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/premium_node).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/premium_node).


## Table of contents

- Uses
- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Uses

- Install Modules from extends or commands.

- Step 1 is help where we given procedure about the modules.

- Step 2 is permission where we decide permission all content types or specific content types.

- Step 3 is configure where we decide default text to all content types and specific content types.

- After that we create any node then select none premium options if any content restrict to without login users.

- finaly we check the content of frontend when user is without login.

Note: This module is for Restrict Content for without login users only.
Enable it when ever its needed in the Production Environment.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Customize the menu settings in Administration ? Configuration and modules ?
   Administration ? Administration menu.


## Troubleshooting

If not displayed updated content, check the following:

- Check you have administrator permission to view this module.
- Clear the drupal cache if is not displayed updated content.


## Maintainers

- Raubi Gaur - [raubi gaur](https://www.drupal.org/u/raubi-gaur)
- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Kamlesh Kishor Jha - [Kamlesh Kishor Jha](https://www.drupal.org/u/kamlesh-kishor-jha)
